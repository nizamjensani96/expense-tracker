import React, { useState } from 'react';
import Card from '../UI/Card';
import './NewExpense.css';

const NewExpense = (props) => {
  const [enteredTitle, setEnteredTitle] = useState('');
  const [enteredAmount, setEnteredAmount] = useState('');
  const [enteredDate, setEnteredDate] = useState('');
  const [isFormShown, setIsFormShown] = useState(false);

  const titleChangeHandler = (e) => {
    setEnteredTitle(e.target.value);
  };
  const ampountChangeHandler = (e) => {
    setEnteredAmount(e.target.value);
  };
  const dateChangeHandler = (e) => {
    setEnteredDate(e.target.value);
  };
  const formHandler = (e) => {
    e.preventDefault();
    const newExpense = {
      id: Math.floor(Math.random().toString()),
      title: enteredTitle,
      amount: +enteredAmount,
      date: new Date(enteredDate),
    };
    props.onAddNewExpense(newExpense);
    setEnteredTitle('');
    setEnteredAmount('');
    setEnteredDate('');

    setIsFormShown(false);
  };
  const addNewExpenseHandler = () => {
    setIsFormShown(true);
  };
  const cancelHandler = () => {
    setIsFormShown(false);
  };

  let formExpense;
  if (isFormShown) {
    formExpense = (
      <form onSubmit={formHandler}>
        <div className="new-expense__controls">
          <div className="new-expense__control">
            <label>Title</label>
            <input
              type="text"
              onChange={titleChangeHandler}
              value={enteredTitle}
            ></input>
          </div>
          <div className="new-expense__control">
            <label>Amount</label>
            <input
              type="number"
              onChange={ampountChangeHandler}
              value={enteredAmount}
              min="0.01"
              step="0.01"
            ></input>
          </div>
          <div className="new-expense__control">
            <label>Date</label>
            <input
              type="date"
              min="2019-01-01"
              max="2023-12-31"
              onChange={dateChangeHandler}
              value={enteredDate}
            ></input>
          </div>
        </div>
        <div className="new-expense__actions">
          <button type="submit" onClick={cancelHandler}>
            Cancel
          </button>
          <button type="submit">Add Expense</button>
        </div>
      </form>
    );
  }

  return (
    <Card className="new-expense">
      <button type="submit" onClick={addNewExpenseHandler}>
        Add New Expense
      </button>
      {formExpense}
    </Card>
  );
};

export default NewExpense;
