import React from 'react';
import ExpenseDate from './ExpenseDate';
import './ExpenseItem.css';
import Card from '../UI/Card';
const ExpenseItem = (props) => {
  if (props.items.length === 0) {
    return <h2 className="expenses-list__fallback">No expense found!</h2>;
  }
  return (
    <div>
      {props.items.map((expense) => (
        <Card className="expense-item" key={expense.id}>
          <ExpenseDate date={expense.date} />
          <div className="expense-item__description">
            <h2>{expense.title}</h2>
            <p className="expense-item__price">RM{expense.amount}</p>
          </div>
        </Card>
      ))}
    </div>
  );
};

export default ExpenseItem;
