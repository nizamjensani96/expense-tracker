import React, { useState } from 'react';
import ExpenseItem from './ExpenseItem';
import './Expenses.css';
import ExpenseFilter from './ExpenseFilter';
import Card from '../UI/Card';
import ExpenseChart from './ExpenseChart';
const Expenses = (props) => {
  const [filteredYear, setFilteredYear] = useState('2023');

  const yearFilterHandler = (selectedYear) => {
    setFilteredYear(selectedYear);
  };

  const filteredExpenses = props.items.filter(
    (expenseYear) => expenseYear.date.getFullYear().toString() === filteredYear
  );

  return (
    <Card className="expenses">
      <ExpenseFilter
        onFilteringYear={yearFilterHandler}
        selectedYear={filteredYear}
      />
      <ExpenseChart expense={filteredExpenses} />
      <ExpenseItem items={filteredExpenses} />
    </Card>
  );
};

export default Expenses;
